<?php

namespace App\Katas;

use App\Katas\Items\BackstagePass;
use App\Katas\Items\Brie;
use App\Katas\Items\Conjured;
use App\Katas\Items\Normal;
use App\Katas\Items\Sulfuras;

class GildedRose
{
    public static function of($name, $quality, $sellIn)
    {   
        $lookup = [
            'normal' => Normal::class,
            'Aged Brie' => Brie::class,
            'Sulfuras, Hand of Ragnaros' => Sulfuras::class,
            'Backstage passes to a TAFKAL80ETC concert' => BackstagePass::class,
            'Conjured Mana Cake' => Conjured::class
        ];

        return new $lookup[$name]($quality, $sellIn);
    }
    
}