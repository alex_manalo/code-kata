<?php

namespace App\Katas;

class StringCalculator {

	const MAX_NUMBER_ALLOWED =1000;

	public function add($numbers)
	{	
		$solution = 0;
		$numbers = $this->parseNumbers($numbers);

		foreach($numbers as $number)
		{
			$this->guardAgainstInvalidNumber($number);

			if($number < self::MAX_NUMBER_ALLOWED){
				$solution += $number;
			}			
		}

		return $solution;
		
	}

	private function guardAgainstInvalidNumber($number)
	{
		if($number < 0) throw new \InvalidArgumentException("Invalid number provided: {$number}");
	}

	private function parseNumbers($numbers)
	{
		return array_map('intval', preg_split('/\s*(,|\\\n)\s*/', $numbers));
	}
}