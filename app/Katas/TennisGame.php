<?php 

namespace App\Katas;

use App\Katas\Player;

class TennisGame {

	public $player1;
	public $player2;
	protected $lookup = [
		0 => 'Love',
		1 => 'Fifteen',
		2 => 'Thirty',
		3 => 'Forty'
	];

	public function __construct(Player $player1, Player $player2)
	{
		$this->player1 = $player1;
		$this->player2 = $player2;
	}

	public function score()
	{	
		if($this->hasAWinner())
		{
			return $this->winner()->name . " wins game!";
		}

		if($this->hasAdvantage())
		{
			return "Advantage " . $this->winner()->name . '.';
		}

		if($this->isDeuce())
		{
			return "Deuce.";
		}

		return $this->generalScore();
	}

	private function isTied()
	{
		return $this->player1->points == $this->player2->points;
	}

	private function hasAdvantage()
	{
		return $this->hasEnoughPointsToBeWon() && $this->isLeadingByOne();
	}

	private function hasAWinner()
	{
		return $this->hasEnoughPointsToBeWon() && $this->isLeadingByTwo();
	}

	private function isDeuce()
	{
		return $this->player1->points + $this->player2->points >= 6 && $this->isTied();
	}

	private function hasEnoughPointsToBeWon()
	{
		return max([$this->player1->points, $this->player2->points]) >= 4;
	}

	private function isLeadingByOne()
	{
		return abs($this->player1->points - $this->player2->points) == 1;	
	}

	private function isLeadingByTwo()
	{
		return abs($this->player1->points - $this->player2->points) >= 2;	
	}

	private function winner()
	{
		return $this->player1->points > $this->player2->points ? $this->player1 : $this->player2;
	}

	private function generalScore()
	{
		$score = $this->lookup[$this->player1->points] . '-';
		$score .= $this->isTied() ? 'All' : $this->lookup[$this->player2->points];

		return $score;
	}
}