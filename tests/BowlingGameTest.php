<?php

use App\Katas\BowlingGame;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BowlingGameTest extends TestCase
{

	/** @test */
	public function it_scores_a_gutter_game_as_zero()
	{
		$bowl = new BowlingGame;

		$this->rollTimes($bowl, 20, 0);		

		$this->assertEquals($bowl->getScore(),0);
	}

	/** @test */
	public function it_scores_all_the_knocked_down_pins_for_a_game()
	{
		$bowl = new BowlingGame;

		$this->rollTimes($bowl, 20, 1);		

		$this->assertEquals($bowl->getScore(),20);
	}

	/** @test */
	public function it_awards_a_one_score_bonus_for_every_spare()
	{
		$bowl = new BowlingGame;

		$bowl->roll(2);
		$bowl->roll(8);
		$bowl->roll(5);

		$this->rollTimes($bowl, 17, 0);		

		$this->assertEquals($bowl->getScore(),20);
	}

	/** @test */
	public function it_awards_a_two_roll_bonus_for_a_strike_in_a_previous_frame()
	{
		$bowl = new BowlingGame;

		$bowl->roll(10);
		$bowl->roll(7);
		$bowl->roll(2);

		$this->rollTimes($bowl, 17, 0);

		$this->assertEquals($bowl->getScore(),28);	
	}

	/** @test */
	public function it_scores_a_perfect_game()
	{
		$bowl = new BowlingGame;		

		$this->rollTimes($bowl, 12, 10);

		$this->assertEquals($bowl->getScore(), 300);	
	}

	private function rollTimes($bowl, $count, $pins)
	{
		for($i = 0; $i < $count; $i++)
		{
			$bowl->roll($pins);	
		}
	}
}
