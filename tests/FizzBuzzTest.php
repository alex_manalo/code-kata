<?php

use App\Katas\FizzBuzz;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class FizzBuzzTest extends TestCase
{

	/** @test */
	public function it_tanslates_1_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(1);

		$this->assertEquals($result, 1);
	}

	/** @test */
	public function it_tanslates_2_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(2);

		$this->assertEquals($result, 2);
	}

	/** @test */
	public function it_tanslates_3_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(3);

		$this->assertEquals($result, 'fizz');
	}

	/** @test */
	public function it_tanslates_5_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(5);

		$this->assertEquals($result, 'buzz');
	}

	/** @test */
	public function it_tanslates_9_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(9);

		$this->assertEquals($result, 'fizz');
	}

	/** @test */
	public function it_tanslates_10_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(10);

		$this->assertEquals($result, 'buzz');
	}

	/** @test */
	public function it_tanslates_15_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(15);

		$this->assertEquals($result, 'fizzbuzz');
	}

	/** @test */
	public function it_tanslates_99_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->execute(99);

		$this->assertEquals($result, 'fizz');
	}

	/** @test */
	public function it_translates_a_sequence_of_numbers_for_fizzbuzz()
	{
		$fizzbuzz = new FizzBuzz;

		$result = $fizzbuzz->executeUpTo(5);

		$this->assertEquals($result, [1, 2, 'fizz', 4, 'buzz']);
	}
}
