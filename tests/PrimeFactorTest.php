<?php

use App\Katas\PrimeFactor;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class PrimeFactorTest extends TestCase
{
    /** @test */
    public function it_returns_an_empty_array_on_1()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(1);

    	$this->assertEquals($result, []);
    }

    /** @test */
    public function it_returns_2_for_2()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(2);

    	$this->assertEquals($result, [2]);
    }

    /** @test */
    public function it_returns_3_for_3()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(3);

    	$this->assertEquals($result, [3]);
    }

    /** @test */
    public function it_returns_2_2_for_4()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(4);

    	$this->assertEquals($result, [2,2]);
    }

    /** @test */
    public function it_returns_5_for_5()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(5);

    	$this->assertEquals($result, [5]);
    }

    /** @test */
    public function it_returns_2_3_for_6()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(6);

    	$this->assertEquals($result, [2,3]);
    }

    /** @test */
    public function it_returns_2_2_2_for_8()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(8);

    	$this->assertEquals($result, [2,2,2]);
    }

    /** @test */
    public function it_returns_3_3_for_9()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(9);

    	$this->assertEquals($result, [3,3]);
    }

    /** @test */
    public function it_returns_2_2_5_5_for_100()
    {
    	$prime = new PrimeFactor;

    	$result = $prime->generate(100);

    	$this->assertEquals($result, [2,2,5,5]);
    }
}
