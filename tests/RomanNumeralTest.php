<?php

use App\Katas\RomanNumeral;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RomanNumeralTest extends TestCase
{
    /** @test **/
    public function it_returns_I_for_1()
    {
    	$roman = new RomanNumeral;

    	$result = $roman->convert(1);

    	$this->assertEquals($result, 'I');
    }

    /** @test **/
    public function it_returns_II_for_2()
    {
    	$roman = new RomanNumeral;

    	$result = $roman->convert(2);

    	$this->assertEquals($result, 'II');
    }

    /** @test **/
    public function it_returns_V_for_5()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(5);

        $this->assertEquals($result, 'V');
    }

    /** @test **/
    public function it_returns_X_for_10()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(10);

        $this->assertEquals($result, 'X');
    }

    /** @test **/
    public function it_returns_XV_for_15()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(15);

        $this->assertEquals($result, 'XV');
    }

    /** @test **/
    public function it_returns_XX_for_20()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(20);

        $this->assertEquals($result, 'XX');
    }

    /** @test **/
    public function it_returns_L_for_50()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(50);

        $this->assertEquals($result, 'L');
    }

    /** @test **/
    public function it_returns_XLIX_for_49()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(49);

        $this->assertEquals($result, 'XLIX');
    }

    /** @test **/
    public function it_returns_C_for_100()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(100);

        $this->assertEquals($result, 'C');
    }

    /** @test **/
    public function it_returns_D_for_500()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(500);

        $this->assertEquals($result, 'D');
    }

    /** @test **/
    public function it_returns_M_for_1000()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(1000);

        $this->assertEquals($result, 'M');
    }

    /** @test **/
    public function it_returns_MCMXCIX_for_1999()
    {
        $roman = new RomanNumeral;

        $result = $roman->convert(1999);

        $this->assertEquals($result, 'MCMXCIX');
    }
}
