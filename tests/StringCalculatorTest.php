<?php

use App\Katas\StringCalculator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class StringCalculatorTest extends TestCase
{
	/** @test */
	public function it_translates_an_empty_string_to_zero()
	{
		$calc = new StringCalculator;

		$result = $calc->add("");

		$this->assertEquals($result, 0);
	}

	/** @test */
	public function it_finds_the_sum_of_one_number()
	{
		$calc = new StringCalculator;

		$result = $calc->add('5');

		$this->assertEquals($result, 5);
	}

	/** @test */
	public function it_finds_the_sum_of_two_numbers()
	{
		$calc = new StringCalculator;

		$result = $calc->add("1,2");

		$this->assertEquals($result, 3);
	}

	/** @test */
	public function it_finds_the_sum_of_any_amount_of_numbers()
	{
		$calc = new StringCalculator;

		$result = $calc->add("1,2,3,4,5");

		$this->assertEquals($result, 15);
	}

	/** @test */
	public function it_ignores_numbers_greater_than_one_thousand()
	{
		$calc = new StringCalculator;

		$result = $calc->add("2,2,1000,5");

		$this->assertEquals($result, 9);
	}

	/** @test */
	public function it_allows_for_new_line_delimeters()
	{
		$calc = new StringCalculator;

		$result = $calc->add('2\n2\n10,20');

		$this->assertEquals($result, 34);
	}

	/**
     * @expectedException InvalidArgumentException
     */
	public function it_disallows_negative_number()
	{
		$calc = new StringCalculator;

		$result = $calc->add("3,-5");

		$this->setExpectedException(new InvalidArgumentException('Invalid number provided: -5'));
	}
}
