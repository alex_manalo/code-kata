<?php

use App\Katas\Player;
use App\Katas\TennisGame;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TennisGameTest extends TestCase
{
	/** @test */
	public function it_scores_a_scoreless_game()
	{	
		$game = new TennisGame(new Player('John', 0), new Player('Jane', 0));

		$score = $game->score();

		$this->assertEquals($score, 'Love-All');
	}

	/** @test */
	public function it_scores_a_1_0_game()
	{
		$game = new TennisGame(new Player('John', 1), new Player('Jane', 0));

		$score = $game->score();

		$this->assertEquals($score, 'Fifteen-Love');
	}

	/** @test */
	public function it_scores_a_2_0_game()
	{
		$game = new TennisGame(new Player('John', 2), new Player('Jane', 0));
		
		$score = $game->score();

		$this->assertEquals($score, 'Thirty-Love');
	}

	/** @test */
	public function it_scores_a_3_0_game()
	{
		$game = new TennisGame(new Player('John', 3), new Player('Jane', 0));
		
		$score = $game->score();

		$this->assertEquals($score, 'Forty-Love');
	}

	/** @test */
	public function it_scores_a_4_0_game()
	{
		$game = new TennisGame(new Player('John', 4), new Player('Jane', 0));
		
		$score = $game->score();

		$this->assertEquals($score, 'John wins game!');
	}

	/** @test */
	public function it_scores_a_0_4_game()
	{
		$game = new TennisGame(new Player('John', 0), new Player('Jane', 4));
		
		$score = $game->score();

		$this->assertEquals($score, 'Jane wins game!');
	}

	/** @test */
	public function it_scores_a_4_3_game()
	{
		$game = new TennisGame(new Player('John', 4), new Player('Jane', 3));
		
		$score = $game->score();

		$this->assertEquals($score, 'Advantage John.');
	}

	/** @test */
	public function it_scores_a_3_4_game()
	{
		$game = new TennisGame(new Player('John', 3), new Player('Jane', 4));
		
		$score = $game->score();

		$this->assertEquals($score, 'Advantage Jane.');
	}

	/** @test */
	public function it_scores_a_3_3_game()
	{
		$game = new TennisGame(new Player('John', 3), new Player('Jane', 3));
		
		$score = $game->score();

		$this->assertEquals($score, 'Deuce.');
	}
	
	/** @test */
	public function it_scores_a_8_8_game()
	{
		$game = new TennisGame(new Player('John', 8), new Player('Jane', 8));
		
		$score = $game->score();

		$this->assertEquals($score, 'Deuce.');
	}

	/** @test */
	public function it_scores_a_8_10_game()
	{
		$game = new TennisGame(new Player('John', 8), new Player('Jane', 10));
		
		$score = $game->score();

		$this->assertEquals($score, 'Jane wins game!');
	}	


}
